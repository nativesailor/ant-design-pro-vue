import request from '@/utils/request'

const api = {
  user: '/user',
  role: '/role',
  service: '/service',
  app_list: 'https://native-dev.yunative.cn/native-app-center/v1/app/pageListApp',
  app_add: 'https://native-dev.yunative.cn/native-app-center/v1/app/createOrUpdateApp',
  app_delete: 'https://native-dev.yunative.cn/native-app-center/v1/app/deleteApp',
  pipeline_trigger: 'https://native-dev.yunative.cn/native-app-center/v1/pipeline/trigger',
  // app_list: `http://${process.env.NATIVE_URL}/native-app-center/v1/app/pageListApp`,
  // app_add: `http://${process.env.NATIVE_URL}/native-app-center/v1/app/createOrUpdateApp`,
  // app_delete: `http://${process.env.NATIVE_URL}/native-app-center/v1/app/deleteApp`,
  // pipeline_trigger: `http://${process.env.PIPELINE_URL}/pipeline`,
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree'
}

export default api

export function getUserList (parameter) {
  return request({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function getRoleList (parameter) {
  return request({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList (parameter) {
  return request({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getAppList (parameter) {
  const { pageNo, pageSize, ...rest } = parameter // 解构出 pageNo 和其他参数
  return request({
    url: api.app_list,
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    params: {
      pageSize: pageSize,
      pageNo: pageNo - 1 // 将 pageNo 减一后设置到请求体中
    },
    data: {
      ...rest
    }
  })
}

export function getPermissions (parameter) {
  return request({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return request({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

export function saveApp (parameter) {
  return request({
    url: api.app_add,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function deleteApp (id) {
  return request({
    url: api.app_delete,
    method: 'delete',
     params: { id }
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function saveSub (sub) {
  return request({
    url: '/sub',
    method: sub.id === 0 ? 'post' : 'put',
    data: sub
  })
}

export function triggerPipeline (data) {
  return request({
    url: api.pipeline_trigger,
    method: 'post',
    data: data
  })
}
