FROM nginx

RUN rm /etc/nginx/conf.d/default.conf

ADD deploy/nginx.conf /etc/nginx/conf.d/default.conf
COPY dist/ /usr/share/nginx/html/

# yarn run build     
# docker build -t dockerhub-dev.yunative.cn/native-web:v0.0.3 . 
# docker push dockerhub-dev.yunative.cn/native-web:v0.0.3